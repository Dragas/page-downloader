package lt.saltyjuice.dragas.pagedownloader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Implements builder pattern for zip files and exposes the end result as a byte array
 */
public class ZipBuilder {
    /**
     * stores file url in key and contents of that file as value.
     */
    private final Map<String, byte[]> fileMap = new TreeMap<>();

    public ZipBuilder addEntry(String filename, byte[] data) {
        fileMap.put(filename, data);
        return this;
    }

    public byte[] build() throws IOException {
        ByteArrayOutputStream baout = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(baout);
        for (Map.Entry<String, byte[]> fileEntry : fileMap.entrySet()) {
            System.err.println("Writing file " + fileEntry.getKey());
            ZipEntry entry = new ZipEntry(fileEntry.getKey());
            zout.putNextEntry(entry);
            zout.write(fileEntry.getValue());
        }
        zout.closeEntry();
        zout.close();
        return baout.toByteArray();
    }
}
