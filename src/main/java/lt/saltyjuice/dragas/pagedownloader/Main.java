package lt.saltyjuice.dragas.pagedownloader;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Main {
    private static ChromeDriverService service;
    private static RemoteWebDriver driver;
    private static JDA jda;

    public static void main(String[] args) throws InterruptedException, IOException, LoginException {
        service = new ChromeDriverService
                .Builder()
                .usingAnyFreePort()
                .usingDriverExecutable(new File("/chromedriver/chromedriver"))
                .withVerbose(true)
                .build();
        service.start();
        driver = new RemoteWebDriver(service.getUrl(), new ChromeOptions().addArguments("--no-sandbox", "--headless", "--disable-gpu", "--window-size=1920,1200", "--ignore-certificate-errors", "--whitelist-ips"));
        jda = new JDABuilder(args[0]).addEventListeners(new TMListener()).build();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            driver.quit();
            service.stop();
        }));
    }

    public static ZippedFile fetchAsZip(String url) throws InterruptedException, IOException {
        driver.get(url);
        Thread.sleep(TimeUnit.SECONDS.toMillis(15));
        String pageName = driver.getTitle().replace(":", "");
        driver.executeScript("Array.from(document.getElementsByTagName('script')).forEach((it) => it.remove())");
        driver.executeScript("Array.from(document.querySelectorAll('input')).forEach((it) => it.setAttribute('value', it.value))");
        driver.executeScript("Array.from(document.querySelectorAll('textarea')).forEach((it) => it.innerHTML = it.value)");
        driver.executeScript("document.getElementById('consent_container').remove()");
        /*List<String> styleSheetsToDownload = driver.findElementsByCssSelector("link[rel='stylesheet']")
                .stream()
                .map((it) -> it.getAttribute("href"))
                .collect(Collectors.toList());*/
        List<LinkPair> pairs = driver.findElementsByCssSelector("link[rel='stylesheet']")
                .stream()
                .map((it) -> {
                    String href = it.getAttribute("href");
                    String preformattedHref = extractCssLink(href);
                    preformattedHref = String.format("%s/%s", "styles", preformattedHref).replace("//", "/");
                    driver.executeScript("arguments[0].href = arguments[1]", it, preformattedHref);
                    if (href.indexOf("/") == 0)
                        href = url + href;
                    return new LinkPair(href, preformattedHref);
                })
                .collect(Collectors.toList());
        ZipBuilder builder = new ZipBuilder();
        builder.addEntry(String.format("%s.html", pageName), driver.getPageSource().getBytes());
        for (LinkPair pair : pairs) {
            String original = pair.getOriginal();
            String local = pair.getLocal();
            driver.get(original);
            System.out.println(driver.getPageSource());
            byte[] src = driver.findElementByTagName("pre").getText().getBytes();
            builder.addEntry(local, src);
        }
        return new ZippedFile(pageName + ".zip", builder.build());
    }

    private static String extractCssLink(String href) {
        int questionMarkIndex = href.indexOf("?");
        if (questionMarkIndex >= 0)
            href = href.substring(0, questionMarkIndex);
        int protocolSeparatorIndex = href.indexOf("//");
        if (protocolSeparatorIndex >= 0) {
            href = href.substring(protocolSeparatorIndex + 2);
            int tldSeparator = href.indexOf("/");
            if (tldSeparator >= 0)
                href = href.substring(tldSeparator);
        }
        return href;
    }

    private static final class LinkPair {
        private final String original;
        private final String local;

        private LinkPair(String original, String local) {
            this.original = original;
            this.local = local;
        }

        public String getOriginal() {
            return original;
        }

        public String getLocal() {
            return local;
        }
    }
}
