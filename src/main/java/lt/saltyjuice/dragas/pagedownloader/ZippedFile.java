package lt.saltyjuice.dragas.pagedownloader;

public class ZippedFile {
    private final byte[] data;
    private final String filename;

    public ZippedFile(String filename, byte[] data) {
        this.filename = filename;
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }

    public String getFilename() {
        return filename;
    }
}
