package lt.saltyjuice.dragas.pagedownloader;

import net.dv8tion.jda.api.entities.IMentionable;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class TMListener extends ListenerAdapter {
    private final AtomicBoolean lock = new AtomicBoolean(false);
    private static final Logger LOG = LoggerFactory.getLogger(TMListener.class);
    private static final ExecutorService executor = Executors.newScheduledThreadPool(2);


    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        executor.execute(() -> attemptPageSync(event));
    }

    private void attemptPageSync(MessageReceivedEvent event) {
        LOG.info("Received message: {}", event.getMessage().getContentRaw());
        IMentionable currentUser = event.getJDA().getSelfUser();
        String id = currentUser.getId();
        if (event.getMessage().isMentioned(currentUser)) {
            String content = event.getMessage().getContentRaw().replaceFirst(
                    String.format(
                            "<@!?%s> ", id
                    ), "");
            LOG.info("New content: {}", content);
            if (content.startsWith("sync")) {
                if (lock.compareAndSet(false, true)) {
                    content = content.replace("sync ", "");
                    LOG.info("New content: {}", content);
                    event.getChannel().sendMessage("Fetching...").queue();
                    try {
                        ZippedFile zip = Main.fetchAsZip(content);
                        String filename = zip.getFilename();
                        byte[] data = zip.getData();
                        event.getChannel().sendFile(data, filename).queue(this::unlock);
                    } catch (InterruptedException | IOException e) {
                        String errorMessage = String.format("@Dragas#1580 An error occurred! %s", UUID.randomUUID());
                        TMListener.LOG.error(errorMessage, e);
                        event.getChannel().sendMessage(errorMessage).queue(this::unlock);
                    }
                } else {
                    event.getChannel().sendMessage("Failed to get a lock on Chrome instance. There might be a sync still in progress!").queue();
                }
            }
        }
    }

    private void unlock(Message message) {
        this.lock.compareAndSet(true, false);
    }
}
