FROM openjdk:11.0.5-jdk-stretch

RUN mkdir /chromedriver
RUN wget https://chromedriver.storage.googleapis.com/79.0.3945.36/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip -d /chromedriver
ENV PATH="${PATH}:/chromedriver"
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN apt-get update
RUN apt-get install apt-transport-https ca-certificates apt-utils -y
RUN echo "deb https://dl.google.com/linux/chrome/deb/ stable main" | tee -a /etc/apt/sources.list.d/google.list
RUN apt-get update
RUN apt-get install google-chrome-stable -y

CMD ["bash"]
WORKDIR /wdir
USER runner:runner
